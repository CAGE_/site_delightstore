<div id="search" class="input-group">
  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-ls" />
  <span class="input-group-btn">
    <button type="button" class="btn btn-primary btn-ls"><i class="fa fa-search"></i></button>
  </span>
</div>