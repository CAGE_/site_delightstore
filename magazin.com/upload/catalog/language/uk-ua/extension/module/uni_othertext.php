<?php
$_['text_search_all'] = 'Скрізь';
$_['text_search_phrase'] = 'Наприклад: ';
$_['text_live_search_all'] = 'Всі результати пошуку';
$_['text_live_search_empty'] = 'Нічого не знайдено';
$_['text_manufacturer_more'] = 'Детальніше...';
$_['text_image'] = 'Зображення';
$_['text_attributes'] = 'Характеристики';
$_['text_description'] = 'Опис';
$_['text_more'] = 'детальніше...';
$_['text_contact_data'] = 'Контактні дані';
$_['text_header_callback'] = 'Не додзвонилися? ';
$_['text_header_callback1'] = 'Замовлення дзвінка!';
$_['button_compact'] = 'Компактний';
$_['text_stock_indicator'] = 'Наявність:';
$_['text_short_attributes'] = 'Короткі характеристики';
$_['text_all_attributes'] = 'Всі характеристики';
$_['text_special_timer'] = 'До закінчення акції:';
$_['filter_pro_heading'] = 'Фільтр';
$_['text_admin_reply'] = 'Відповідь адміністрації:';
$_['text_plus'] = 'Переваги:';
$_['text_minus'] = 'Недоліки:';
$_['text_comment'] = 'Коментар:';
$_['entry_plus'] = 'Переваги:';
$_['entry_minus'] = 'Недостатки:';
$_['error_plus'] = 'Опис достоїнств товару не може бути менше трьох літер';
$_['error_minus'] = 'Опис недоліків товару не може бути менше трьох літер';
$_['add_new_review'] = 'Відправити свій відгук';
$_['text_contacts'] = 'Контакти';
$_['text_location'] = 'Схема проїзду';
$_['text_callback'] = 'Замовлення дзвінка';
$_['text_send'] = 'Відправити';
$_['text_success_new'] = '<div class="row">
								<div class="col-xs-3"><img src="%s" class="img-responsive"></div>
								<div class="col-xs-9">%s</div>
							</div>
							<div class="row">
								<div class="col-xs-6 text-left"><a onclick="$(\'#modal-cart\').modal(\'hide\')"><span>Продовжити покупки</span></a></div>
								<div class="col-xs-6 text-right"><button class="btn btn-primary" onclick="location=\'%s\'">Оформити замовлення</button></div>
							</div>';
$_['text_quickorder_button'] = 'Оформити замовлення';
$_['text_special_day'] = 'Днів';
$_['text_special_hour'] = 'Годин';
$_['text_special_min'] = 'Хвилин';
$_['text_special_sec'] = 'Секунд';
$_['text_quick_order_success'] = 'Спасибі за Ваше замовлення. Ми зв\'яжемося з вами через кілька хвилин.';
$_['text_quick_order_error'] = 'Обов\'язково вкажіть своє ім\'я та контактний телефон.';
$_['text_new_user_email'] = 'Ваш логін: %s';
$_['text_new_user_password'] = 'Ваш пароль: %s';
$_['text_new_discount'] = 'Знижки від кількості';
$_['text_error'] = 'Запитувана сторінка не знайдена';
$_['text_no_product'] = 'Не знайдено товарів, що відповідають критеріям фільтра';
$_['button_show_more'] = 'Показати ще';
$_['text_fly_wishlist'] = 'Закладки';
$_['text_fly_compare'] = 'Список порівняння';
$_['text_length'] = 'Габарити (ДхШхВ):';
$_['text_weight'] = 'Вага:';
$_['entry_length_weigth'] = 'Габарити і вага';
$_['text_category'] = 'Категорія:';
$_['text_select_category'] = 'Виберіть підкатегорію';
?>