<?php
$_['text_search_all'] = 'Все категории';
$_['text_search_phrase'] = 'Например: ';
$_['text_live_search_all'] = 'Все результаты поиска';
$_['text_live_search_empty'] = 'Ничего не найдено';
$_['text_manufacturer_more'] = 'Подробнее...';
$_['text_image'] = 'Изображение';
$_['text_attributes'] = 'Характеристики';
$_['text_description'] = 'Описание';
$_['text_category'] = 'Категория:';
$_['text_length'] = 'Габариты (ДхШхВ):';
$_['text_weight'] = 'Вес:';
$_['entry_length_weigth'] = 'Габариты и вес';
$_['text_more'] = 'подробнее...';
$_['text_contact_data'] = 'Контактные данные';
$_['text_header_callback'] = 'Не дозвонились? ';
$_['text_header_callback1'] = 'Заказ звонка!';
$_['button_compact'] = 'Компактный';
$_['button_show_more'] = 'Показать еще';
$_['text_stock_indicator'] = 'Наличие:';
$_['text_short_attributes'] = 'Краткие характеристики';
$_['text_all_attributes'] = 'Все характеристики';
$_['text_special_timer'] = 'До окончания акции:';
$_['filter_pro_heading'] = 'Фильтр';
$_['text_new_discount'] = 'Скидки от количества';
$_['text_admin_reply'] = 'Ответ администрации:';
$_['text_plus'] = 'Достоинства:';
$_['text_minus'] = 'Недостатки:';
$_['text_comment'] = 'Комментарий:';
$_['entry_plus'] = 'Достоинства:';
$_['entry_minus'] = 'Недостатки:';
$_['error_plus'] = 'Описание достоинств товара не может быть менее трех букв';
$_['error_minus'] = 'Описание недостатков товара не может быть менее трех букв';
$_['add_new_review'] = 'Отправить свой отзыв';
$_['text_contacts'] = 'Контакты';
$_['text_location'] = 'Схема проезда';
$_['text_callback'] = 'Заказ звонка';
$_['text_send'] = 'Отправить';
$_['text_success_new'] = '<div class="row">
								<div class="col-xs-3"><img src="%s" class="img-responsive" /></div>
								<div class="col-xs-9">%s</div>
							</div>
							<div class="row">
								<div class="col-xs-6 text-left"><a data-dismiss="modal"><span>Продолжить покупки</span></a></div>
								<div class="col-xs-6 text-right"><button class="btn btn-primary" onclick="location=\'%s\'">Оформить заказ</button></div>
							</div>';
$_['text_quick_order_success'] = 'Спасибо за Ваш заказ. Мы свяжемся с вами через несколько минут.';
$_['text_quick_order_error'] = 'Обязательно укажите своё имя и контактный телефон.';
$_['text_quickorder_button'] = 'Оформить заказ';
$_['text_special_day'] = 'Дней';
$_['text_special_hour'] = 'Часов';
$_['text_special_min'] = 'Минут';
$_['text_special_sec'] = 'Секунд';
$_['text_new_user_email'] = 'Ваш логин: %s';
$_['text_new_user_password'] = 'Ваш пароль: %s';
$_['text_fly_wishlist'] = 'Закладки';
$_['text_fly_compare'] = 'Список сравнения';
$_['text_error'] = 'Запрашиваемая страница не найдена';
$_['text_no_product'] = 'Не найдено товаров, соответствующих критериям фильтра';
$_['text_select_category'] = 'Выберите подкатегорию';
?>