<?php
$_['heading_title']   		= 'Новости';
$_['text_error']      		= 'Пока нет ни одной новости';
$_['text_error_news']      	= 'К сожалению новость не найдена';
$_['text_error_category']   = 'К сожалению категория новостей не найдена';
$_['text_more']  			= 'Читать далее';
$_['text_posted'] 			= 'Добавлено: ';
$_['text_viewed'] 			= '(%s просмотров) ';
$_['text_related_product_title'] = 'Связанные товары';
$_['button_news']     		= 'Вернуться к списку новостей';
$_['button_continue']  		= 'Продолжить';
?>
