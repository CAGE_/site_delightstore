<?php
$_['text_search_all'] = 'All';
$_['text_search_phrase'] = 'For example: ';
$_['text_live_search_all'] = 'All search results';
$_['text_live_search_empty'] = 'Nothing found';
$_['text_manufacturer_more'] = 'Read more...';
$_['text_image'] = 'Image';
$_['text_attributes'] = 'Features';
$_['text_description'] = 'Description';
$_['text_more'] = 'read more...';
$_['text_contact_data'] = 'Contact details';
$_['text_header_callback'] = 'Didn\'t get through. ';
$_['text_header_callback1'] = 'Ordering call!';
$_['button_compact'] = 'Compact';
$_['text_stock_indicator'] = 'Availability:';
$_['text_short_attributes'] = 'A brief description';
$_['text_all_attributes'] = 'All the features';
$_['text_special_timer'] = 'Before the end of the action:';
$_['filter_pro_heading'] = 'Filter';
$_['text_admin_reply'] = 'The administration\'s response:';
$_['text_plus'] = 'Advantages:';
$_['text_minus'] = 'Disadvantages:';
$_['text_comment'] = 'Comment:';
$_['entry_plus'] = 'Advantages:';
$_['entry_minus'] = 'Недостатки:';
$_['error_plus'] = 'The description of the advantages of the product can not be less than three letters';
$_['error_minus'] = 'Description of product defects, can not be less than three letters';
$_['add_new_review'] = 'Send your feedback';
$_['text_contacts'] = 'Contacts';
$_['text_location'] = 'Location map';
$_['text_callback'] = 'Ordering a call';
$_['text_send'] = 'Send';
$_['text_success_new'] = '<div class="row">
								<div class="col-xs-3"><img src="%s" class="img-responsive"></div>
								<div class="col-xs-9">%s</div>
							</div>
							<div class="row">
								<div class="col-xs-6 text-left"><a onclick="$(\'#modal-cart\').modal(\'hide\')"><span>Continue shopping</span></a></div>
								<div class="col-xs-6 text-right"><button class="btn btn-primary" onclick="location=\'%s\'">Checkout</button></div>
							</div>';
$_['text_quick_order_success'] = 'Thank you for Your order. We will contact you in a few minutes.';
$_['text_quick_order_error'] = 'Be sure to provide your name and contact phone number.';
$_['text_quickorder_button'] = 'To order';
$_['text_special_day'] = 'Days';
$_['text_special_hour'] = 'Hours';
$_['text_special_min'] = 'Minutes';
$_['text_special_sec'] = 'Seconds';
$_['text_new_user_email'] = 'Your login: %s';
$_['text_new_user_password'] = 'Ваш пароль: %s';
$_['text_new_discount'] = 'Discounts from the number';
$_['text_error'] = 'Requested page was not found';
$_['text_no_product'] = 'Not found the products that meet the filter criteria';
$_['button_show_more'] = 'Show more';
$_['text_fly_wishlist'] = 'Wishlist';
$_['text_fly_compare'] = 'The comparison list';
$_['text_length'] = 'Dimensions (LxWxH):';
$_['text_weight'] = 'Weight:';
$_['entry_length_weigth'] = 'Dimensions and weight';
$_['text_category'] = 'Category:';
$_['text_select_category'] = 'Select a subcategory';
?>