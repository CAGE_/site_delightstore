<?php
$_['heading_title'] = 'News';
$_['text_error'] = 'While there is no news';
$_['text_error_news'] = 'Unfortunately the news is not found';
$_['text_error_category'] = 'Unfortunately news category not found';
$_['text_more'] = 'Read more...';
$_['text_posted'] = 'Added: ';
$_['text_viewed'] = '(%s hits) ';
$_['text_related_product_title'] = 'Related products';
$_['button_news'] = 'Back to news list';
$_['button_continue'] = 'Continue';
?>