<?php
$_['text_additional'] = 'If you have a coupon code for a discount or the bonus points that you want to use, select the appropriate option.';
$_['text_coupon'] = 'Enter the coupon code';
$_['text_reward'] = 'Use points';
$_['text_voucher'] = 'Enter certificate code';
$_['text_loading'] = 'Loading...';
$_['text_total_checkout'] = 'The amount of your order: ';
$_['button_confirm_checkout'] = 'All the information is correct, place your order';
$_['button_apply'] = 'To apply';
$_['text_checkout'] = 'Ordering';
$_['text_product_weight'] = 'The weight of the items in the basket: ';
$_['button_remove'] = 'To remove';
$_['shipping_address'] = 'Shipping address';
$_['select_country'] = 'Select a country';
$_['text_delivery_methods'] = 'Shipping methods';
$_['text_payment_methods'] = 'Payment methods';
$_['text_comment2'] = 'Comment';
$_['text_login2'] = 'Log in with your username and password';
$_['text_register3'] = 'To register';
$_['text_register2'] = 'Already registered?';
$_['text_cart_empty'] = 'Your basket is empty';
?>