$(function() {
	if(uniJsVars.fly_menu) {
		fly_menu_enabled = 1;
	
		if($(window).width() > 992 && !$('#unicheckout').length) {
	
			$('#menu_wrap').remove();
		
			var menu = $('header #menu'), search = $('header #search').html(), name = $('h1.heading').html(), price = $('#product .price').html(), btn = $('#product').find('.add_to_cart');
			var phone = $('header #phone').html(), account = $('#top #account').html(), cart = $('header #cart').html();
		
			var product = uniJsVars.fly_menu_product;
	
			if(product && $('#product').length) {
				var html = '<div class="product_wrap col-md-8 col-lg-8 col-xxl-12"><div><div class="product_name">'+name+'</div>';
				html += '<div class="price">'+price+'</div>';
				html += '<button type="button" class="'+btn.attr('class').replace('btn-lg', '')+'">'+btn.html()+'</button></div></div>';
			} else {
				html = '<div class="menu_wrap col-md-3 col-lg-3 col-xxl-4"><div id="menu" class="menu1">'+menu.html()+'</div></div>';
				html += '<div id="search_w" class="search_wrap col-md-5 col-lg-5 col-xxl-8"><div id="search3">'+search+'</div></div>';
			}
			html += '<div class="phone_wrap col-md-2 col-lg-2 col-xxl-4"><div id="phone">'+phone+'</div></div>';
			html += '<div class="account_wrap col-md-1 col-lg-1 col-xxl-2"><div id="account">'+account+'</div></div>';
			html += '<div class="cart_wrap col-md-1 col-lg-1 col-xxl-2"><div id="cart">'+cart+'</div></div>';
	
			$(window).scroll(function(){
				if(!$('#menu_wrap').length) {
					if($(this).scrollTop() > 100) {
						uniAddCss('catalog/view/theme/unishop2/stylesheet/flymenu.css');
			
						$('html body').append('<div id="menu_wrap"><div class="container"><div class="row">'+html+'</div></div></div>');
	
						if(menu.attr('class').substr(0, 5) == 'menu2') {
							$('#menu_wrap #menu').addClass('menu3');
						}
						
						uniMenuAim.init();
						if(uniJsVars.menu_blur) uniMenuBlur();
						
						if(product && $('#product').length) {
							$('body').on('change', '#product input', function() {
								setTimeout(function() { 
									$('#menu_wrap .product_wrap .price').html($('#product .price').html());
								}, 300);
							});
	
							$('.product_wrap button').click(function() {
								$('#button-cart').click();
							});
			
							$('#menu_wrap .product_name').mouseover(function () {
								var boxWidth = $(this).width();
									
								$text = $('#menu_wrap .product_name span');
								$textWidth = $('#menu_wrap .product_name span').width();

								if ($textWidth > boxWidth) {
									$($text).animate({left: -(($textWidth+20) - boxWidth)}, 500);
								}
							}).mouseout(function () {
								$($text).stop().animate({left: 0}, 500);
							});
						}
					}
				} else {
					if($(this).scrollTop() > 190) {
						$('#menu, #menu_wrap').addClass('show');
					} else {
						$('#menu, #menu_wrap').removeClass('show');
					}
				}
			});
		}
	} else {
		fly_menu_enabled = 0;
	}

	if(uniJsVars.fly_cart && !fly_menu_enabled) {
		$(window).scroll(function(){		
			if($(window).width() > 992) {
				$(this).scrollTop() > 200 ? $('#cart').addClass('fly') : $('#cart').removeClass('fly');
			}
		});
	}
});