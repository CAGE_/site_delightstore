<?php
class ControllerExtensionModuleUniProductBanner extends Controller {
	public function index() {
		$uniset = $this->config->get('config_unishop2');
		$lang_id = $this->config->get('config_language_id');
		
		$data['product_banner_position'] = $uniset['product_banner_position'];
		
		$data['product_banners'] = [];
		
		if(isset($uniset[$lang_id]['product_banners'])) {
			foreach($uniset[$lang_id]['product_banners'] as $banner) {
				if($banner['text']) {
					$data['product_banners'][] = array(
						'icon' 			=> $banner['icon'],
						'text' 			=> html_entity_decode($banner['text'], ENT_QUOTES, 'UTF-8'),
						'link' 			=> $banner['link'],
						'link_popup' 	=> isset($banner['link_popup']) ? true : false,
						'hide' 			=> isset($banner['hide']) ? true : false,
					);
				}
			}
		}
		
		return $this->load->view('extension/module/uni_product_banner', $data);
	}
}
?>
