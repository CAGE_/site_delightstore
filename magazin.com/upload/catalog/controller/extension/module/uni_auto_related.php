<?php  
class ControllerExtensionModuleUniAutoRelated extends Controller {
	public function index() {
		
		static $module = 0;
		
		$this->load->model('catalog/product');
		$this->load->model('extension/module/uni_related');
		$this->load->model('tool/image');
		$this->load->model('extension/module/uni_new_data');

		$this->language->load('product/product');
		$this->load->language('extension/module/uni_othertext');		
		
		$uniset = $this->config->get('config_unishop2');
		$lang_id = $this->config->get('config_language_id');
		
		$data['heading_title'] = $uniset[$lang_id]['title_similar'];
		
		$data['show_quick_order_text'] = isset($uniset['show_quick_order_text']) ? $uniset['show_quick_order_text'] : '';			
		$data['quick_order_icon'] = isset($uniset['show_quick_order']) ? html_entity_decode($uniset[$lang_id]['quick_order_icon'], ENT_QUOTES, 'UTF-8') : '';	
		$data['quick_order_title'] = isset($uniset['show_quick_order']) ? $uniset[$lang_id]['quick_order_title'] : '';
		$data['show_rating'] = isset($uniset['show_rating']) ? true : false;
		$data['show_rating_count'] = isset($uniset['show_rating_count']) ? true : false;
		$data['wishlist_btn_disabled'] = isset($uniset['wishlist_btn_disabled']) ? true : false;
		$data['compare_btn_disabled'] = isset($uniset['compare_btn_disabled']) ? true : false;
		$data['show_attr_name'] = isset($uniset['show_attr_name']) ? true : false;
		$data['show_options'] = isset($uniset['show_options']) ? true : false;
		
		$stock = isset($uniset['stock_similar']) ? true : false;
			
		$img_width = $this->config->get('theme_'.$this->config->get('config_theme') . '_image_related_width');
		$img_height = $this->config->get('theme_'.$this->config->get('config_theme') . '_image_related_height');
		
		$currency = $this->session->data['currency'];
		
		$data['products'] = [];
		
		$product_id = isset($this->request->get['product_id']) ? (int)$this->request->get['product_id'] : 0;
		
		$results = $this->model_extension_module_uni_related->getAutoRelated($product_id, $uniset['product_similar'], $stock);
		
		if ($results) {			
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $img_width, $img_height);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $img_width, $img_height);
				}
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $currency);
				} else {
					$price = false;
				}
						
				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $currency);
				} else {
					$special = false;
				}
				
				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
			
				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $currency);
				} else {
					$tax = false;
				}
				
				$new_data = $this->model_extension_module_uni_new_data->getNewData($result);
				$show_description = isset($uniset['show_description']) && !isset($uniset['show_description_alt']) || isset($uniset['show_description_alt']) && !$new_data['attributes'] ? true : false;
				
				if($new_data['special_date_end']) {
					$data['show_timer'] = true;
				}	
				
				if($result['quantity'] > 0) {
					$show_quantity = isset($uniset['show_quantity_cat']) ? true : false;
					$cart_btn_icon = $uniset[$lang_id]['cart_btn_icon'];
					$cart_btn_text = $uniset[$lang_id]['cart_btn_text'];
					$cart_btn_class = '';
					$quick_order = isset($uniset['show_quick_order']) ? true : false;
				} else {
					$show_quantity = isset($uniset['show_quantity_cat_all']) ? true : false;
					$cart_btn_icon = $uniset[$lang_id]['cart_btn_icon_disabled'];
					$cart_btn_text = $uniset[$lang_id]['cart_btn_text_disabled'];
					$cart_btn_class = $uniset['cart_btn_disabled'];
					$quick_order = isset($uniset['show_quick_order_quantity']) ? true : false;
				}
							
				$data['products'][] = array(
					'product_id' 		=> $result['product_id'],
					'thumb'   	 		=> $image,
					'name'    			=> $result['name'],
					'description' 		=> utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_'.$this->config->get('config_theme') . '_product_description_length')) . '..',
					'tax'         		=> $tax,
					'price'   	 		=> $price,
					'special' 	 		=> $special,
					'rating'     		=> $rating,
					'reviews'    		=> sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
					'href'    	 		=> $this->url->link('product/product', 'product_id='.$result['product_id'], true),
					'additional_image'	=> $new_data['additional_image'],
					'stickers' 			=> $new_data['stickers'],
					'num_reviews' 		=> isset($uniset['show_rating_count']) ? $result['reviews'] : '',
					'special_date_end' 	=> $new_data['special_date_end'],
					'minimum' 			=> $result['minimum'] ? $result['minimum'] : 1,
					'quantity_indicator'=> $new_data['quantity_indicator'],
					'price_value' 		=> $this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax'))*$this->currency->getValue($currency),
					'special_value' 	=> $this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax'))*$this->currency->getValue($currency),
					'attributes' 		=> $new_data['attributes'],
					'options'			=> $new_data['options'],
					'show_description'	=> $show_description,
					'show_quantity'		=> $show_quantity,
					'cart_btn_icon'		=> $cart_btn_icon,
					'cart_btn_text'		=> $cart_btn_text,
					'cart_btn_class'	=> $cart_btn_class,
					'quick_order'		=> $quick_order,
				);
			}
		}
		
		$data['module'] = $module++;
		
		return $this->load->view('extension/module/uni_auto_related', $data);
	}
}
?>