<?php
// HTTP
define('HTTP_SERVER', 'http://magazin.com/admin/');
define('HTTP_CATALOG', 'http://magazin.com/');

// HTTPS
define('HTTPS_SERVER', 'http://magazin.com/admin/');
define('HTTPS_CATALOG', 'http://magazin.com/');

// DIR
define('DIR_APPLICATION', 'C:/server/OSPanel/domains/magazin.com/admin/');
define('DIR_SYSTEM', 'C:/server/OSPanel/domains/magazin.com/system/');
define('DIR_IMAGE', 'C:/server/OSPanel/domains/magazin.com/image/');
define('DIR_LANGUAGE', 'C:/server/OSPanel/domains/magazin.com/admin/language/');
define('DIR_TEMPLATE', 'C:/server/OSPanel/domains/magazin.com/admin/view/template/');
define('DIR_CONFIG', 'C:/server/OSPanel/domains/magazin.com/system/config/');
define('DIR_CACHE', 'C:/server/OSPanel/domains/magazin.com/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/server/OSPanel/domains/magazin.com/system/storage/download/');
define('DIR_LOGS', 'C:/server/OSPanel/domains/magazin.com/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/server/OSPanel/domains/magazin.com/system/storage/modification/');
define('DIR_UPLOAD', 'C:/server/OSPanel/domains/magazin.com/system/storage/upload/');
define('DIR_CATALOG', 'C:/server/OSPanel/domains/magazin.com/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'magazin');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
