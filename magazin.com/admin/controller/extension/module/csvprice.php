<?php 
class ControllerModuleCSVPrice extends Controller { 
	private $_name = 'csvprice';
	private $_version = '0.1.5b';
	private $error = array();
	
	public function index() {		
		$this->load->language('module/csvprice');
		$this->load->model('module/csvprice');
		$this->document->setTitle($this->language->get('heading_title'));
		
		$data['csvprice_version'] = $this->_version;
				
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			if (is_uploaded_file($this->request->files['import']['tmp_name'])) {
				$this->model_module_csvprice->import($this->request->files['import']['tmp_name']);
			} else {
				$this->error['warning'] = $this->language->get('error_empty');
			}
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_select_all'] = $this->language->get('text_select_all');
		$data['text_unselect_all'] = $this->language->get('text_unselect_all');
		$data['text_module'] = $this->language->get('text_module');
		$data['text_notes'] = $this->language->get('text_notes');
		
		$data['entry_import'] = $this->language->get('entry_import');
		$data['entry_import_help'] = $this->language->get('entry_import_help');
		$data['entry_export'] = $this->language->get('entry_export');
		$data['entry_category'] = $this->language->get('entry_category');
		$data['entry_category_help'] = $this->language->get('entry_category_help');
		$data['text_type_ex1'] = $this->language->get('text_type_ex1');
		$data['text_type_ex2'] = $this->language->get('text_type_ex2');
				 
		$data['button_export'] = $this->language->get('button_export');
		$data['button_import'] = $this->language->get('button_import');
		
		$data['tab_general'] = $this->language->get('tab_general');
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$data['token'] = $this->session->data['token'];

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();
		foreach ($languages as $language) {
			if (isset($this->error['code' . $language['language_id']])) {
				$data['error_code' . $language['language_id']] = $this->error['code' . $language['language_id']];
			} else {
				$data['error_code' . $language['language_id']] = '';
			}
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/'.$this->_name, 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$data['import'] = $this->url->link('module/csvprice', 'token=' . $this->session->data['token'], 'SSL');
		$data['export'] = $this->url->link('module/csvprice/export', 'token=' . $this->session->data['token'], 'SSL');
			
		$this->load->model('catalog/category');
		$data['categories'] = $this->model_catalog_category->getCategories(0);
				
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('module/csvprice.tpl', $data));	

	}
	
	public function export() {
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			
			$price_export = date("Ymd-Hi").'_price_export.csv';
			
			$this->response->addheader('Pragma: public');
			$this->response->addheader('Expires: 0');
			$this->response->addheader('Content-Description: File Transfer');
			$this->response->addheader('Content-Type: application/octet-stream');
			$this->response->addheader('Content-Disposition: attachment; filename='.$price_export);
			$this->response->addheader('Content-Transfer-Encoding: binary');
			
			$this->load->model('module/csvprice');
			
			if ( ! isset($this->request->post['product_category'])) {
				$product_category = NULL;
			} else {
				$product_category = $this->request->post['product_category'];
			}
			
			if ($this->request->post['type_of_export']=='0') {
				$this->response->setOutput($this->model_module_csvprice->export($product_category));
			} else {
				$this->response->setOutput($this->model_module_csvprice->exportc($product_category));
			}
			
		} else {
			return $this->forward('error/permission');
		}
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/category')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}
?>