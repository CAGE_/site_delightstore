<?php
// Heading
$_['heading_title']    = 'Import Plugin';

// Text
$_['text_success']     = 'Success: You have successfully imported your data!';

// Entry
$_['entry_import']     = 'Import';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Import Plugin!';
$_['error_export']     = 'Warning: You must select at least one table to export!';
$_['error_empty']      = 'Warning: The file you uploaded was empty!';