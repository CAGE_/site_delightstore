<?php
// Heading
$_['heading_title']    = 'CSV Price import/export';

// Text
$_['text_export']      = 'Сделать дамп файл с ценами';
$_['text_success']     = 'Данные импортированы!';
$_['text_type_ex1']     = 'Экспортировать в формате: <kbd>[ID]</kbd> ; <kbd>[Наименование]</kbd> ; <kbd>[Модель]</kbd> ; <kbd>[Производитель]</kbd> ; <kbd>[Кол-во]</kbd> ; <kbd>[Цена]</kbd>';
$_['text_type_ex2']     = 'Экспортировать в формате: <kbd>[Модель]</kbd> ; <kbd>[Наименование]</kbd> ; <kbd>[Производитель]</kbd> ; <kbd>[Кол-во]</kbd> ; <kbd>[Цена]</kbd>';
$_['text_module']     = 'Модули';
$_['text_notes']     = '<strong>Импорт поддерживает следующие форматы:</strong> <br />
<kbd>[ID товара]</kbd> ; <kbd>[Наименование товара]</kbd> ; <kbd>[Модель товара]</kbd> ; <kbd>[Производитель]</kbd> ; <kbd>[Кол-во товара]</kbd> ; <kbd>[Цена товара]</kbd> - соответствует формату экспорта, обновление по <span class="text-primary">[ID товара]</span><br />
<kbd>[Модель товара]</kbd> ; <kbd>[Кол-во товара]</kbd> ; <kbd>[Цена товара]</kbd> - обновление по <span class="text-primary">[Модель товара]</span><br />
<kbd>[Модель товара]</kbd> ; <kbd>[Цена товара]</kbd>	- обновление по <span class="text-primary">[Модель товара]</span>, обновляется только цена товара
<br/ ><br/ >Разделитель полей знак "<span class="text-primary"><strong>точка с запятой</strong></span>", разделитель строк "<span class="text-primary"><strong>перенос строки</strong></span>". <br /> При импорте данных обновляются только количество и цена товара<p class="text-primary">Данный модуль не удаляет и не добавляет товар!</p>';

// Entry
$_['entry_import']		= 'Импорт данных из файла:';
$_['entry_import_help']		= 'Исходный файл должен быть в кодировке Windows-1251';
$_['entry_export']		= 'Экспорт:';
$_['entry_category']	= 'Экспорт из категорий:';
$_['entry_category_help']	= 'Если категории не выбраны - экспортирует все товары, <br>файл выгружается в кодировке Windows-1251';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_empty']      = 'Загруженный файл пуст!';

// Button
$_['button_export']		= 'Экспорт';
$_['button_import']		= 'Импорт';
?>