<?php
// Heading
$_['heading_title']    = 'Import easy';

// Text
$_['text_success']     = 'Импорт прошло успешно!';

// Entry
$_['entry_import']     = 'Импорт товаров';

// Error
$_['error_permission'] = 'У вас нет прав для импорта!';
$_['error_export']     = 'Для резервного копирования необходимо выбрать хотя бы одну таблицу!';
$_['error_empty']      = 'Внимание: Загруженный файл пустой!';