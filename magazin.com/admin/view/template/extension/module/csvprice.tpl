<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
	<div class="row">
	<div class="col-md-8">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
	</div>
	<div class="col-md-4" style="text-align: right;">
	  <a onclick="$('#import').submit();" class="btn btn-primary"><span><i class="fa fa-upload"></i> <?php echo $button_import; ?></span></a>
	  <a onclick="$('#export').submit();" class="btn btn-primary"><span><i class="fa fa-download"></i> <?php echo $button_export; ?></span></a>
	</div>
	</div>
    </div>
  </div>
  
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-upload"></i><?php echo $entry_import; ?></h3>
      </div>
	  
      <form action="<?php echo $import; ?>" method="post" enctype="multipart/form-data" id="import">
        <div class="row" style="margin: 5px;">
			<div class="col-sm-3">
            <span class="help"><?php echo $entry_import_help; ?></span>
			</div>
			<div class="col-sm-9" >
			<div class="row" style="margin: 5px;">
            <div class="col-md-6" ><input class="" type="file" name="import" style="margin-top: 7px;"  /></div>
            <div class="col-md-6" style="text-align: right;" ></div>
			</div>
			</div>
			
        </div>
      </form>
	  <div class="clearfix;" style="padding: 10px;"><?php echo $text_notes; ?></div>
	  
	  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-download"></i><?php echo $entry_export; ?></h3>
      </div>
	  
      <form action="<?php echo $export; ?>" method="post" enctype="multipart/form-data" id="export">
		<div class="row"><div class="col-md-3">
			<p style="padding: 10px;"><?php echo $entry_category_help; ?></p>
		</div>
		<div class="col-md-9"><p >
			<input style="margin: 10px;" type="radio" name="type_of_export" value="0"><?php echo $text_type_ex1; ?><br>
			<input style="margin: 10px;" type="radio" name="type_of_export" value="1" checked="true"><?php echo $text_type_ex2; ?><br>
		</p></div></div>
      <table class="table table-striped table-bordered">
            
              
                  <?php $class = 'odd'; ?>
                  <?php foreach ($categories as $category) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
				  <tr>
					<td class="text-center"><input type="checkbox" name="product_category[]" value="<?php echo $category['category_id']; ?>" /></td>
                    <td><?php echo $category['name']; ?></td>
                  
				  </tr>
                  <?php } ?>
                
            
            </table>
      </form>
    </div>

</div>
<?php echo $footer; ?>